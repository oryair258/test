import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AngularFireModule } from 'angularfire2';

import { AppComponent } from './app.component';
import {RouterModule,Routes} from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UsersComponent } from './users/users.component';
import { UserComponent } from './user/user.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { UsersService } from './users/users.service';
import { UserFormComponent } from './user-form/user-form.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { InvoicesFormComponent } from './invoices-form/invoices-form.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { InvoicesService } from './invoices/invoices.service';
import { ProductsComponent } from './products/products.component';
import { ProductsService } from './products/products.service';
import { ProductComponent } from './product/product.component';
import { PostsComponent } from './posts/posts.component';
import { PostsService } from './posts/posts.service';
import { ApisComponent } from './apis/apis.component';
import { ApisService } from './apis/apis.service';
import { CustomersComponent } from './customers/customers.component';
import {CustomersService} from './customers/customers.service';
import { CustomerComponent } from './customer/customer.component';

export const firebaseconfig = {
    apiKey: "AIzaSyAop-ZZ3RDt-wxbK7QLkq_ESu_hoI2o0mY",
    authDomain: "test-ledogma.firebaseapp.com",
    databaseURL: "https://test-ledogma.firebaseio.com",
    storageBucket: "test-ledogma.appspot.com",
    messagingSenderId: "463751350158"
  }

const appRoutes:Routes = [
{path:'customers', component: CustomersComponent},
{path:'apis', component: ApisComponent},
{path:'', component: CustomersComponent},
{path:'**', component: PageNotFoundComponent},
]

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    UsersComponent,
    UserComponent,
    SpinnerComponent,
    UserFormComponent,
    InvoicesComponent,
    InvoicesFormComponent,
    InvoiceComponent,
    ProductsComponent,
    ProductComponent,
    PostsComponent,
    ApisComponent,
    CustomersComponent,
    CustomerComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseconfig)

  ],
  providers: [UsersService,InvoicesService,ProductsService,PostsService,ApisService,CustomersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
