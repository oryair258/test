import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {AngularFire} from 'angularfire2';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';

@Injectable()
export class CustomersService {

private _url='http://orya.myweb.jce.ac.il/api/customers';

  
  getCustomersFromApi() {
       return this._http.get(this._url).map(res =>res.json()).delay(2000);
    }


  deleteCustomer(customer){
    customer.remove();
  }
  
  constructor(private af:AngularFire, private _http:Http) { }

}
