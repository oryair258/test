import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {User} from './user';

@Component({
  selector: 'jce-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  inputs:['user']
})
export class UserComponent implements OnInit {

 @Output() deleteEvent = new EventEmitter<User>();
 @Output() editEvent = new EventEmitter<User>();
 

user:User;
tempUser:User = {email:null,name:null};
isEdit : boolean = false;
editButtonText = 'Edit';

  constructor() { }

 sendDelete(){
    this.deleteEvent.emit(this.user);
  }

  cancelEdit(){
    this.isEdit = false;
    this.user.email = this.tempUser.email;
    this.user.name = this.tempUser.name;
    this.editButtonText = 'Edit'; 
  }


toggleEdit(){
     //update parent about the change
     this.isEdit = !this.isEdit; 
     this.isEdit ?  this.editButtonText = 'Save' : this.editButtonText = 'Edit';   
     if(this.isEdit){
       this.tempUser.email = this.user.email;
       this.tempUser.name = this.user.name;
     } else {
       this.editEvent.emit(this.user);
     }
  }

  ngOnInit() {
  }

}