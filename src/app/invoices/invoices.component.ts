import { Component, OnInit } from '@angular/core';
import { InvoicesService } from './invoices.service';
import {Invoice} from '../invoice/invoice';


@Component({
  selector: 'jce-invoices',
  templateUrl: './invoices.component.html',
  //styleUrls: ['./invoices.component.css']
      styles: [`
    .invoices li {cursor: default; }
    .invoices li:hover { background: #ecf0f1; }
    .list-group-item.active,
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
  `]
})
export class InvoicesComponent implements OnInit {

isLoading:Boolean=true;
cuurentInvoice;
invoices;

 addInvoice(invoices){
   this._invoicesService.addInvoice(invoices);
 }  
 constructor(private _invoicesService:InvoicesService) { }

 select (invoice){
   this.cuurentInvoice=invoice;
   console.log(this.cuurentInvoice);
 }
 
  ngOnInit(){
     this._invoicesService.getInvoices().subscribe(invoicesData => 
   {this.invoices = invoicesData;
   this.isLoading=false});
    

  }

}
