import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Product} from './product';


@Component({
  selector: 'jce-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  inputs: ['product']
})
export class ProductComponent implements OnInit {

@Output() deleteEvent = new EventEmitter<Product>();
product:Product;


  sendDelete(){
  this.deleteEvent.emit(this.product);
}
  constructor() { }

  ngOnInit() {
  }

}
