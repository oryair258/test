import { Injectable } from '@angular/core';
import {AngularFire} from 'angularfire2';

@Injectable()
export class ProductsService {

  productsObservable;

  getProducts(){
      this.productsObservable =this.af.database.list('/product').map(
        products=>{
          products.map(
            product=>{
              product.produtCat = [];
                product.produtCat.push(
                  this.af.database.object('/category/' + product.categoryId)
                )  }   );
          return products;
        }       )
return this.productsObservable;  
   }

   deleteProduct(product){
     let productKey = product.$key;
    this.af.database.object('/product/' + productKey).remove();
  }


  constructor(private af:AngularFire) { }

}
