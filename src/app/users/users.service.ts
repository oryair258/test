import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {AngularFire} from 'angularfire2';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/map';


@Injectable()
export class UsersService {

usersObservable;
// private _url='http://jsonplaceholder.typicode.com/users';

  getUsers(){
      this.usersObservable =this.af.database.list('/users');
	return this.usersObservable;
    }  

	addUser(user){
    this.usersObservable.push(user);
    }

    updateUser(user){
      let userKey = user.$key;
      let userdate = {name: user.name, email:user.email}
      this.af.database.object('/users/'+ userKey).update(userdate);
    }

    deleteUser(user){
      let userKey = user.$key;
      this.af.database.object('/users/'+ userKey).remove();
    }

constructor(private af:AngularFire) { }
}