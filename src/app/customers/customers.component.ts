import { Component, OnInit } from '@angular/core';
import {CustomersService} from './customers.service';
import {Customer} from '../customer/customer';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
 // styleUrls: ['./customers.component.css']
       styles: [`
    .customers li {cursor: default; }
    .customers li:hover { background: #ecf0f1; }
    .newclass { background-color:yellow; }
    .list-group-item.active,
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
  `]

})
export class CustomersComponent implements OnInit {

customers;
cuurentCustomer;
isLoading:Boolean=true;

 select (customer){
   this.cuurentCustomer=customer;
   console.log(this.cuurentCustomer);
 }

 deleteCustomer(customer){
   this.customers.splice(
      this.customers.indexOf(customer),1
   )
   }

  constructor(private _customersService: CustomersService) { }

  ngOnInit() {
      this._customersService.getCustomersFromApi().subscribe(customersData => 
      {this.customers = customersData.result;
        this.isLoading=false});
      
  }

}
