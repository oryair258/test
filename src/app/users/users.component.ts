import { Component, OnInit } from '@angular/core';
import {UsersService} from './users.service';

@Component({
  selector: 'jce-users',
  templateUrl: './users.component.html',
  //styleUrls: ['./users.component.css'],
  styles: [`
	    .users li { cursor: default; }
	    .users li:hover { background: #ecf0f1; }
	    .list-group-item.active, 
	    .list-group-item.active:hover { 
	         background-color: #ecf0f1;
	         border-color: #ecf0f1; 
	         color: #2c3e50;
	    }     
	  `]

})
export class UsersComponent implements OnInit {

  users;
  currentUser;
  isLoading:Boolean = true;
	


constructor(private _usersService:UsersService) { }

select(user){
  this.currentUser=user;
}

 deleteUser(originalAndEdited){
   this.users.splice(
     this.users.indexOf(originalAndEdited[0],1,originalAndEdited[1])
   )
    console.log(this.users);
  }

addUser(user){
   this._usersService.addUser(user);
 }
  
  editUser(user){
     this._usersService.updateUser(user); 
  }  

 ngOnInit() {
        this._usersService.getUsers()
			    .subscribe(users => {this.users = users;
                               this.isLoading = false;
                               console.log(users)});
  }

}