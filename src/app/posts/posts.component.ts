import { Component, OnInit } from '@angular/core';
import {PostsService} from './posts.service';


@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
posts;


constructor(private _postsService:PostsService) { }

  ngOnInit() {
  this._postsService .getPosts().subscribe(postData =>this.posts = postData);

  }

}
