import { Component, OnInit } from '@angular/core';
import {ApisService} from './apis.service';

@Component({
  selector: 'app-apis',
  templateUrl: './apis.component.html',
  styleUrls: ['./apis.component.css']
})
export class ApisComponent implements OnInit {

apis;

  constructor(private _apisService: ApisService) { }

  ngOnInit() {
    this._apisService.getApisFromApi().subscribe(apisData => {this.apis = apisData.result;
          console.log(this.apis)});
  }

}
