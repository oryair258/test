import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {NgForm} from '@angular/forms';
import {Invoice} from '../invoice/invoice';
import {InvoicesService} from '../invoices/invoices.service';

@Component({
  selector: 'jce-invoices-form',
  templateUrl: './invoices-form.component.html',
  styleUrls: ['./invoices-form.component.css']
})
export class InvoicesFormComponent implements OnInit {
invoices;
invoice:Invoice = {name:'', amount:1000} ;




onSubmit(form:NgForm){
 console.log(this.invoice);
 this._invoicesService.addInvoice(this.invoice);
 this.invoice={name:'',amount:1000};}
 

 
  constructor(private _invoicesService:InvoicesService) { }

 ngOnInit(){
     this._invoicesService.getInvoices().subscribe(invoicesData => 
   {this.invoices = invoicesData});

}
}