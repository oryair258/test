import { Component, OnInit, Output, EventEmitter } from '@angular/core';
 import {Customer} from './customer';

@Component({
  selector: 'jce-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css'],
  inputs: ['customer']
})
export class CustomerComponent implements OnInit {

@Output() deleteEvent = new EventEmitter<Customer>();

  customer: Customer;
  
  sendDelete(){
  this.deleteEvent.emit(this.customer);
}

  constructor() { }

  ngOnInit() {
  }

}
