import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class PostsService {

 private _url='http://jsonplaceholder.typicode.com/posts'; 

getPosts(){
      return this._http.get(this._url).map(res =>res.json());  
     }  
  constructor(private _http:Http) { }


}
